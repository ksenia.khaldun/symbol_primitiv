Object.prototype[Symbol.toPrimitive] = function (hint) {
    console.log(`${hint}`)
    if (hint === 'string') {
        if (isPrim(this.toString())) {
            return this.toString();
        } else if (isPrim(this.valueOf())) {
            return this.valueOf();
        } else {
            throw new TypeError('Cannot convert object to primitive value');
        };
    } else {
        if (isPrim(this.valueOf())) {
            return this.valueOf();
        } else if (isPrim(this.toString())) {
            return this.toString();
        } else {
            throw new TypeError('Cannot convert object to primitive value');
        };

    }
}
function isPrim(value){
    return typeof value !=='object'
}


let a = [3];
let b = [4]


console.log(a - b)//number
console.log(a * b);// number
console.log(a > b);// number
console.log(String(a));//string
console.log(a + "a")//default
console.log(a+b)//default

